<?php 
	require('conexion.php');
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Section 1</title>
	<style>
		form{
			text-align: center;
		}
		input, select{
			font-size: 16px;
			margin-top: 10px;
		}
		input[type="submit"]{
			margin-top: 20px;
		}
	</style>
</head>
<body>
	
	<form action="" method="post" autocomplete="off">
		<input type="text" name="nombre" id="nombre" placeholder="Nombre"><br>
		<input type="text" name="apellido" id="apellido" placeholder="Apellido"><br>
		<input type="text" name="cedula" id="cedula" placeholder="Cedula"><br>
		<select name="escuela">
			<option value="Ing. de Sistemas">Ing. de Sistemas</option>
			<option value="Ing. Mant. Mecanico">Ing. Mant. Mecanico</option>
			<option value="Ing. Electrica">Ing. Electrica</option>
			<option value="Ing. Electronica">Ing. Electronica</option>
			<option value="Ing. Civil">Ing. Civil</option>
			<option value="Ing. Industrial">Ing. Industrial</option>
			<option value="Arquitectura">Arquitectura</option>
		</select><br>
		<input type="submit" name="registrar" value="Registrar">
		<input type="submit" name="mostrar" value="Mostrar Registros">
		<input type="submit" name="eliminar" value="Eliminar Registros">
		<input type="submit" name="actualizar" value="Actualizar Registro">
	</form>

<?php 

	if(isset($_POST['registrar'])){

		$nombre = $_POST['nombre'];
		$apellido = $_POST['apellido'];
		$cedula = $_POST['cedula'];
		$escuela = $_POST['escuela'];

		mysqli_set_charset($link, "utf-8");

		$insert_query = "INSERT INTO 
				  datos (nombre, apellido, cedula, escuela)
				  VALUES
				  ('$nombre', '$apellido', $cedula, '$escuela');
				  ";

		$insert_results = mysqli_query($link, $insert_query);

		if($insert_results){
			echo "registro guardado";
		}else{
			echo "error en el registro";
		}

	} // ./registrar

	if(isset($_POST['mostrar'])){

		$show_query = "SELECT * FROM datos;";

		$show_results = mysqli_query($link, $show_query);

		while($row=mysqli_fetch_assoc($show_results)){

			printf("%s %s %u (%s)", $row['nombre'], $row['apellido'], $row['cedula'], $row['escuela']);
			echo "</br>";

		}

	} // ./mostrar

	if(isset($_POST['eliminar'])){
		$nombre = mysqli_real_escape_string($link, $_POST['nombre']);
		$delete_query = "DELETE FROM datos WHERE nombre='$nombre';";

		$delete_results = mysqli_query($link, $delete_query);

		if(mysqli_affected_rows($link)==0){
			echo "no hay registros que coincidan con la busqueda";
		}
		else{
			echo "eliminados " . mysqli_affected_rows($link) . " registros";
		}
	} // ./eliminar

	// update
	if(isset($_POST['actualizar'])){
		$cedula = $_POST['cedula'];

		$update_query = "SELECT * FROM datos WHERE cedula=$cedula;";

		$update_results = mysqli_query($link, $update_query);

		while($row=mysqli_fetch_assoc($update_results)){
			echo '
			
			<form action="update.php" method="post" autocomplete="off">
				<input type="text" name="nombre" id="nombre" value="'.$row['nombre'].'"><br>
				<input type="text" name="apellido" id="apellido" value="'.$row['apellido'].'"><br>
				<input type="text" name="cedula" id="cedula" value="'.$row['cedula'].'" readonly><br>
				<select name="escuela">
					<option value="Ing. de Sistemas">Ing. de Sistemas</option>
					<option value="Ing. Mant. Mecanico">Ing. Mant. Mecanico</option>
					<option value="Ing. Electrica">Ing. Electrica</option>
					<option value="Ing. Electronica">Ing. Electronica</option>
					<option value="Ing. Civil">Ing. Civil</option>
					<option value="Ing. Industrial">Ing. Industrial</option>
					<option value="Arquitectura">Arquitectura</option>
				</select><br>
				<input type="submit" name="actualizar" value="Actualizar Registro">
			</form>

			';
		}
	}
 ?>
</body>
</html>